const express = require("express");
const mongoose = require('mongoose');

const app = express();

// connect to mongodb
mongoose.connect(
  `mongodb://localhost:27017/cesi`,
  {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: true,
    useUnifiedTopology: true,
  }
)
.then(() => console.log(`database connected successfully`))
.catch((err) => console.log(err.message));
mongoose.Promise = global.Promise;

app.use(express.static('public'));
app.use(express.json());

const port = process.env.PORT || 4000;

// initialize routes
app.use('/api',require('./routes/api'));

app.listen(port, () => {
  console.log(`listening on ${port}`);
});